/*
 * LoggerATMega644.c
 *
 * Created: 26/03/2021 15:26:26
 * Author : jo na eh
 */

#include "HtlStddef.h"
#include "Logger.h"
#include "RS232.h"
#include <avr/io.h>
#include <stdio.h>

int customPrintfPut(char character, FILE* file);

TRS232 globalRS232;

int main(void) {
  TRS232Config config;

  config.sendBufferSize = 100;
  config.receiveBufferSize = 10;
  config.baudrate = 9600;
  globalRS232 = RS232Create(ERS232_NO_0, F_CPU, config);

  fdevopen(customPrintfPut, NULL);

  LogInit();

  while (1) {
    printf("My RS232 Test with fdevopen! ");
    DelayMs(500);
  }
}

int customPrintfPut(char character, FILE* file) {
  RS232Send((TRS232)globalRS232, (unsigned char*)&character, 1);
  return 1;
}

