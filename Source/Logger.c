/**
 * File: Logger.c
 *
 * Description:
 *	Test different methods for loggings.
 */

#include "Logger.h"

/******************** Declarations of Private Structs ********************/

typedef struct {
  TLogCallback callback;
  void* userData;
} TLogger;

TLogger logger;

/******************** Definition of Public Functions ********************/

void LogInit(void) { memset(&logger, 0, sizeof(logger)); }

void LogSetLogCallback(TLogCallback callback, void* userData) {
  logger.callback = callback;
  logger.userData = userData;
}

/******************** Definition of Private Functions ********************/

void _LogEvent(const char* file, const char* func, unsigned int line,
               TLogLevel logLevel, const char* format, ...) {
  if (NULL == logger.callback) return;

  TLogEvent logEvent;
  va_list args;

  memset(&logEvent, 0, sizeof(logEvent));

  logEvent.file = file;
  logEvent.function = func;
  logEvent.line = line;
  logEvent.logLevel = logLevel;

  char message[LOGGER_MAX_MESSAGE_SIZE];

  va_start(args, format);
  // see https://en.cppreference.com/w/c/io/vfprintf
  vsnprintf(message, LOGGER_MAX_MESSAGE_SIZE, format, args);
  va_end(args);

  logger.callback(&logEvent, logger.userData, message);
}
