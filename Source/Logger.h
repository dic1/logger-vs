/**
 * File: Logger.h
 *
 * Description:
 *	Test different methods for loggings.
 */

#ifndef LOGGER_H
#define LOGGER_H

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef LOGGER_MAX_MESSAGE_SIZE
#define LOGGER_MAX_MESSAGE_SIZE 256
#endif

typedef enum {
  LOG_LEVEL_DEBUG = 1,
  LOG_LEVEL_INFO = 2,
  LOG_LEVEL_WARNING = 3,
  LOG_LEVEL_ERROR = 4,
} TLogLevel;

typedef struct {
  const char* file;
  const char* function;
  unsigned int line;
  TLogLevel logLevel;
} TLogEvent;

typedef void (*TLogCallback)(const TLogEvent* logEvent, void* userData,
                             const char* message);

/**
 * Function: LogInit
 *
 * Description:
 *	Initialize the Logger. This has to be called once
 *	before any Log functions can be used.
 */
void LogInit(void);

/**
 * Function: LogSetLogCallback
 *
 * Description:
 *	Set a TLogCallback that gets called when
 *	a LogEvent occurs.
 *
 * Parameters:
 *	callback - The custom TLogCallback, which you want to get fired on a
 *             TLogEvent.
 *  userData - Custom data you might want to pass as a parameter.
 */
void LogSetLogCallback(TLogCallback callback, void* userData);

/**
 * Function: _LogEvent
 *
 * Description:
 *	Internal Log function, this should not be used.
 */
void _LogEvent(const char* file, const char* func, unsigned int line,
               TLogLevel logLevel, const char* format, ...);

#ifdef _DEBUG

#define LogDebug(format, ...)                                      \
  _LogEvent(__FILE__, __func__, __LINE__, LOG_LEVEL_DEBUG, format, \
            ##__VA_ARGS__)

#define LogInfo(format, ...) \
  _LogEvent(__FILE__, __func__, __LINE__, LOG_LEVEL_INFO, format, ##__VA_ARGS__)

#define LogWarning(format, ...)                                      \
  _LogEvent(__FILE__, __func__, __LINE__, LOG_LEVEL_WARNING, format, \
            ##__VA_ARGS__)

#else

#define LogDebug(message)
#define LogInfo(message)
#define LogWarning(message)

#endif  // _DEBUG

#define LogError(format, ...)                                      \
  _LogEvent(__FILE__, __func__, __LINE__, LOG_LEVEL_ERROR, format, \
            ##__VA_ARGS__)

#endif  // LOGGER_H
