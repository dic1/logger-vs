/**
 * File: TestLogger.c
 *
 * Description:
 *	Test different methods for loggings on the PC.
 */

#include <Windows.h>
#include <stdio.h>

#include "Logger.h"

#undef LOG_DEBUG_SHOW

void logCallback1Colored(const TLogEvent* logEvent, void* userData,
                         const char* message);
void logCallback2SimplePrintf(const TLogEvent* logEvent, void* userData,
                              const char* message);
void test2AllLogLevels(void);

int main(void) {
  test2AllLogLevels();
  return 0;
}

void logCallback1Colored(const TLogEvent* logEvent, void* userData,
                         const char* message) {
  const HANDLE hConsole = userData;
  char* tag;

  /*
  All colors for SetConsoleTextAttribute.
  Passing 0x1F to SetConsoleTextAttribute() would yield a blue background with
  bright white text.

  0 = Black
  1 = Blue
  2 = Green
  3 = Aqua
  4 = Red
  5 = Purple
  6 = Yellow
  7 = White
  8 = Gray
  9 = Light Blue
  A = Light Green
  B = Light Aqua
  C = Light Red
  D = Light Purple
  E = Light Yellow
  F = Bright White
  */

  switch (logEvent->logLevel) {
    case LOG_LEVEL_DEBUG:
      SetConsoleTextAttribute(hConsole, 0x0F);
      tag = "DEBUG:\n";
      break;

    default:
    case LOG_LEVEL_INFO:
      SetConsoleTextAttribute(hConsole, 0x09);
      tag = "INFO:\n";
      break;

    case LOG_LEVEL_WARNING:
      SetConsoleTextAttribute(hConsole, 0x0E);
      tag = "WARNING:\n";
      break;

    case LOG_LEVEL_ERROR:
      SetConsoleTextAttribute(hConsole, 0x04);
      tag = "ERROR:\n";
      break;
  }

  printf("%sFile: %s Function: %s Line: %u\nMessage: %s\n", tag, logEvent->file,
         logEvent->function, logEvent->line, message);
}

void logCallback2SimplePrintf(const TLogEvent* logEvent, void* userData,
                              const char* message) {
  printf("Message: %s\n", message);
}

void test2AllLogLevels(void) {
  HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

  LogInit();
  LogSetLogCallback(logCallback1Colored, hConsole);

  while (1) {
    LogDebug("my first debug logging: %d\n", 761);
    LogInfo("First info: %s\n", "argument string");
    LogWarning("Warning for the %lu %s\n", 58, "time");
    LogError("System ERROR everything burned\n");
    Sleep(750);
  }
}
